window.addEventListener("load",iniciar,false);
function reset(){
	canvas.width = canvas.width;
}
function Qs(id){return document.querySelector(id)}
function Touch(node,s,e){
	node.addEventListener('touchstart',s);
	node.addEventListener('touchmove',function(ev){ev.preventDefault();});
	node.addEventListener('touchend',e);
	node.addEventListener('touchcancel',e);
}
var iFps = {span:null,fps:0,inc:0,}
var animationFrame = 0;
function frames(){
	if(animationFrame>=60){
		animationFrame%=60;
		iFps.inc++;
		dibujar();
	}
	animationFrame+=Config.fps;
	window.requestAnimationFrame(frames);
}
function iniciar(){
	inzializar();
	setInterval(function(){iFps.span.textContent=iFps.fps=iFps.inc;iFps.inc=0;},1000);
}
var Load = {
	i: 0,
	fun: function (){
		this.i++;
		if(this.i==0)frames();
	},
	add: function (img){
		img.addEventListener('load',function(){Load.fun()});
		this.i--;
	}
}

//--------------------------------
//			Funciones
//--------------------------------
var User = {
	caminando: false,
	direccion: true,
	agachado: false,
	saltando: true,
	fuerzaSalto: 0,
	iSprite: 0,
	on: function (){
		Move.caminando = this.caminando;
		Move.direccion = this.direccion;
		Move.agachado = this.agachado;
		if(!Move.saltando && this.saltando){
			Move.saltando = this.saltando;
			/*...*/Move.fuerzaSalto = this.fuerzaSalto;
		}
		if(this.iSprite > -1){
			Move.iSprite = this.iSprite;
			this.iSprite = -1;
		}
	},
	off: function (){
		this.saltando = Move.saltando;
	}
}
function aB(s){
	if(s){
		User.direccion=false;
		User.caminando=true;
		User.iSprite=2}
	else if(User.direccion==false){
		User.caminando=false;}
}
function bB(s){
	if(s){
		User.direccion=true;
		User.caminando=true;
		User.iSprite=2}
	else if(User.direccion==1){
		User.caminando=false;}
}
function xB(s){
	if(s && !User.saltando){
		User.fuerzaSalto=Config.salto;
		User.saltando = true;
	}
}
function zB(s){
	User.agachado=s;
}
function eventListener() {
	Touch(Qs('#aB'),function(){aB(true)},function(){aB(false)});
	Touch(Qs('#bB'),function(){bB(true)},function(){bB(false)});
	Touch(Qs('#xB'),function(){xB(true)},function(){xB(false)});
	Touch(Qs('#zB'),function(){zB(true)},function(){zB(false)});
}
